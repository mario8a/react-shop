import React, { useRef } from 'react';
import '@styles/CreateAccount.scss';

const CreateAccount = () => {

	const form = useRef(null);

   const handleSubmit = (event) => {
      event.preventDefault();
      const formData = new FormData(form.current);
      const data = {
         username: formData.get('name'),
				 email: formData.get('email'),
         password: formData.get('password')
      }
      console.log(data);
   }

  return (
     <div className="CreateAccount">
        <div className="CreateAccount-container">
           <h1 className="title">My account</h1>
           <form action="/" className="form" ref={form}>
              <div>
                 <label htmlFor="name" className="label">
                    Name
                 </label>
                 <input
                    type="text"
                    name="name"
                    placeholder="Teff"
                    className="input input-name"
                 />
                 <label htmlFor="email" className="label">
                    Email
                 </label>
                 <input
                    type="text"
                    name="email"
                    placeholder="platzi@example.com"
                    className="input input-email"
                 />
                 <label htmlFor="password" className="label">
                    Password
                 </label>
                 <input
                    type="password"
                    name="password"
                    placeholder="*********"
                    className="input input-password"
                 />
              </div>
              <button
                 className="primary-button login-button"
								 onClick={handleSubmit}>
								Create
							</button>
           </form>
        </div>
     </div>
  );
}

export default CreateAccount
