
import React, { useState, useContext, useEffect } from 'react';
import '@styles/Header.scss';

import Menu from '@components/Menu';

import menu from '@icons/icon_menu.svg';
import logo from '@logos/logo_yard_sale.svg';
import shoppingCart from '@icons/icon_shopping_cart.svg';
import AppContext from './../context/AppContext';
import MyOrder from './../containers/MyOrder';
import { Link } from 'react-router-dom';

const Header = () => {
	const [toggle, setToggle] = useState(false);
	const [toggleOrders, setToggleOrders] = useState(false);
	const [offset, setOffset] = useState(0);
	const { state } = useContext(AppContext);

	const handleToogle = () => {
		setToggle(!toggle);
	}

	useEffect(() => {
    window.onscroll = () => {
			if (window.pageYOffset > 500) {
				setToggle(false)
			}
      if (window.pageYOffset > 800) {
				setToggleOrders(false)
			}
    }
  }, []);
	
	return (
		<nav>
			<img src={menu} alt="menu" className="menu" />
			<div className="navbar-left">
				<Link to="/" >
					<img src={logo} alt="logo" className="nav-logo" />
				</Link>
				<ul>
					<li>
						<a href="/">All</a>
					</li>
					<li>
						<a href="/">Clothes</a>
					</li>
					<li>
						<a href="/">Electronics</a>
					</li>
					<li>
						<a href="/">Furnitures</a>
					</li>
					<li>
						<a href="/">Toys</a>
					</li>
					<li>
						<a href="/">Others</a>
					</li>
				</ul>
			</div>
			<div className="navbar-right">
				<ul>
					<li className="navbar-email" onClick={handleToogle}>
						platzi@example.com
					</li>
					<li className="navbar-shopping-cart" onClick={() => setToggleOrders(!toggleOrders)}>
						<img src={shoppingCart} alt="shopping cart" />
						{state.cart.length > 0 ? <div>{state.cart.length}</div> : null} 
					</li>
				</ul>
			</div>
			{ toggle &&  <Menu /> }
			{ toggleOrders && <MyOrder toogleOrder={setToggleOrders} /> }
		</nav>
	);
}

export default Header;