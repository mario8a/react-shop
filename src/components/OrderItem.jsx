import React, { useContext } from 'react';
import '@styles/OrderItem.scss';
import closeIcon from '../assets/icons/icon_close.png'
import AppContext from './../context/AppContext';

const OrderItem = ({product}) => {

	const { removeToCart } = useContext(AppContext);
	
	const handleRemove = product => {
		removeToCart(product)
	}

  return (
    <div className="OrderItem">
			<figure>
				<img src={product.images[0]} alt="bike" alt={product.title}/>
			</figure>
			<p>{product.title}</p>
			<p>${product.price}</p>
			<img src={closeIcon} alt="close"  onClick={() => handleRemove(product)} />
		</div>
  )
}

export default OrderItem
