import { useState } from "react";

const initialState = {
  cart: [],
  toggleContext: false
}

const useInitialState = () => {
  const [state, setState] = useState(initialState);

  const addToCart = (payload) => {
    setState({
      ...state,
      cart: [...state.cart, payload]
    });
  };

  const removeToCart = (payload) => {
    setState({
      ...state,
      cart: state.cart.filter(items => items.id !== payload.id),
    })
  }

  const changeTogle = () => {
    setState({
      ...state,
      toggleContext: !state.toggleContext
    })
  }

  return {
    state,
    addToCart,
    removeToCart,
    changeTogle
  }
}


export default useInitialState;