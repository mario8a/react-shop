import React, { useContext } from 'react';
import OrderItem from '@components/OrderItem';
import '@styles/MyOrder.scss';

import flechita from '@icons/flechita.svg'
import AppContext from '../context/AppContext';
import { Link } from 'react-router-dom';

const MyOrder = ({toogleOrder}) => {
	const { state } = useContext(AppContext);

	const sumTotal = () => {
		const reducer = (acumulador, currentValue) => acumulador + currentValue.price;
		const sum = state.cart.reduce(reducer, 0);
		return sum;
	}

  return (
    <aside className="MyOrder">
			<div className="title-container">
				<img src={flechita} alt="arrow" onClick={() => toogleOrder(false)}/>
				<p className="title">My order</p>
			</div>
			<div className="my-order-content">
				{ state.cart.map(product => (
					<OrderItem product={product} key={`orderItem-${product.id}`}/>
				))}
				<div className="order">
					<p>
						<span>Total</span>
					</p>
					<p>${sumTotal()}</p>
				</div>
				
				<Link to="/checkout">
					<button className="primary-button" onClick={() => toogleOrder(false)}>
						Checkout
					</button>
				</Link>

			</div>
		</aside>
  )
}

export default MyOrder
